#include "visitorder.h"
#include <QDebug>

VisitOrder::VisitOrder(QListWidget *listWidget)
    : listWidget(listWidget)
{
}

void VisitOrder::push_back(Position position, QString tag)
{
    if(curentPos < positions.count())
    {
        for(int i = positions.count(); i > curentPos; --i)
        {
            positions.remove(i-1);
            listWidget->takeItem(i-1);
        }
    }

    if(not positions.isEmpty())
    {
        if(positions[curentPos-1] == position) return;
    }

    listWidget->addItem(tag+"\t"+QString::number(position.lineNumber+1)+"\t"+position.filePath);
    positions.push_back(position);
    curentPos++;
    listWidget->setCurrentRow(curentPos-1);
    print();
}

Position VisitOrder::prev()
{
    if(positions.isEmpty()) throw std::out_of_range{"Nie ma wstecz"};

    if(curentPos-1 > 0)
    {
        curentPos--;
    }

//    qDebug() << "curentPos: " << curentPos;
    listWidget->setCurrentRow(curentPos-1);
    return positions[curentPos-1];
}

Position VisitOrder::next()
{
    if(positions.isEmpty()) throw std::out_of_range{"Nie ma dalej"};

    curentPos++;
    if(curentPos > positions.count())
    {
        curentPos = positions.count();
    }
//        qDebug() << "curentPos: " << curentPos;
        listWidget->setCurrentRow(curentPos-1);
        return positions[curentPos-1];
}

void VisitOrder::clearAll()
{
    positions.clear();
    curentPos = 0;
    listWidget->clear();
}

void VisitOrder::clearFromCurentPosition()
{
    if(curentPos < positions.count())
    {
        for(int i = positions.count(); i > curentPos; --i)
        {
            positions.remove(i-1);
            listWidget->takeItem(i-1);
        }
    }
}

void VisitOrder::print()
{
//    qDebug() << "VisitOrder: ";
    for(auto el : positions)
    {
//        qDebug() << "\t" << el;
    }
}

