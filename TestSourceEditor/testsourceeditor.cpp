#include "testsourceeditor.h"
#include "ui_testsourceeditor.h"
#include <QFileDialog>
#include <QDebug>
#include <QMessageBox>
#include <QTextBlock>
#include <QDockWidget>
#include <QClipboard>
#include <QScrollBar>

TestSourceEditor::TestSourceEditor(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TestSourceEditor),
    settings("MySoft", "TestSourceEditor"),
    tabBarContextMenu(new QMenu(this)),
    getFileNameAction("File name to Clipboard", this),
    getFullPathAction("Full File Path to Clipboard", this)
{
    ui->setupUi(this);
    setWindowTitle("TestSourceEditor alpha_0.2");
    tabBarContextMenu->addAction(&getFileNameAction);
    tabBarContextMenu->addAction(&getFullPathAction);
    tabBarContextMenu->addSeparator();

    connect(&getFileNameAction, &QAction::triggered, this, &TestSourceEditor::getCurrentOpenFileName);
    connect(&getFullPathAction, &QAction::triggered, this, &TestSourceEditor::getCurrentOpenFileFullPath);

    QDockWidget *dockListWidget = new QDockWidget(tr("FunctionList"), this);
    dockListWidget->setAllowedAreas(Qt::RightDockWidgetArea | Qt::BottomDockWidgetArea);

    functionListWidget = new QListWidget(dockListWidget);
    functionListWidget->setEnabled(false);
    dockListWidget->setWidget(functionListWidget);
    addDockWidget(Qt::RightDockWidgetArea, dockListWidget);
    visitOrder = new VisitOrder(functionListWidget);
    tags = new Tags{};

    QFont font;
    font.setFamily("Courier");
    font.setFixedPitch(true);
    font.setPointSize(12);

    codeEditorWidget.setFont(font);
    codeEditorWidget.setLineWrapMode(QPlainTextEdit::NoWrap);

    ui->tabWidget->tabBar()->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->tabWidget->removeTab(0);
    ui->tabWidget->addTab(&codeEditorWidget, "File");
    setCentralWidget(ui->tabWidget);
    this->connect(&codeEditorWidget, &QPlainTextEdit::undoAvailable, this, &TestSourceEditor::undoAvailable);
    this->connect(ui->tabWidget->tabBar(), &QTabBar::customContextMenuRequested, this, &TestSourceEditor::customContextMenuRequested);
}

TestSourceEditor::~TestSourceEditor()
{
    delete tags;
    delete visitOrder;
    delete ui;
}

void TestSourceEditor::on_actionOpen_triggered()
{
    QString lastTestPath = settings.value("lastTestPath").toString();
    lastTestPath = "C:\\Users\\Grzesiek\\Documents\\qt\\test_editor\\przykladoweDane\\MacLinuxRtm\\PsScheduler\\test.flx";

    QString filePath = QFileDialog::getOpenFileName(this, "Open File", lastTestPath);
    if(filePath.isEmpty()) return;

    testFilePath = filePath;
    QString lastPartOfBasePath("MacLinuxRtm");
    int indexOf = filePath.indexOf(lastPartOfBasePath);
    if(indexOf > -1)
    {
        basePath = filePath.left(indexOf+lastPartOfBasePath.size());
        settings.setValue("lastTestPath", filePath);
    }

    visitOrder->clearAll();
    tags->clear();
    findSubInFiles(filePath);
    openFile(Position{filePath});

    statusBar()->showMessage(basePath, 2000);
}

void TestSourceEditor::on_actionGo_to_definition_triggered()
{
    try
    {
        auto tc = codeEditorWidget.textCursor();
        tc.select(QTextCursor::WordUnderCursor);
        auto findDef = tc.selectedText();
//        qDebug() << "Go to def: " << findDef;

        Position positionCurrent{};
        positionCurrent.filePath = currentFilePath;
        positionCurrent.lineNumber = tc.blockNumber();
        positionCurrent.positionInBlock = tc.positionInBlock()-findDef.length();
        Position positionNew = tags->find(findDef);
//        qDebug() << "Finded def: " << position;
        visitOrder->push_back(positionCurrent, findDef);
        visitOrder->push_back(positionNew, findDef);
        openFile(positionNew);
    }
    catch(const std::out_of_range &ex)
    {
        statusBar()->showMessage(ex.what(), 2000);
    }
}

bool TestSourceEditor::findSubInFiles(QString filePath)
{
    if(filePath.isEmpty()) return false;
    qDebug() << "Open file: " << filePath;
    QFile file(filePath);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
//        QMessageBox::warning(this, tr("Application"),
//                             tr("Cannot read file %1:\n%2.")
//                             .arg(QDir::toNativeSeparators(filePath), file.errorString()));

        return false;
    }

    QTextStream in(&file);
    QRegExp require("^\\s*require\\s+(.*)\\s*;");
    QRegExp sub("^\\s*sub\\s+(.*)\\s*");
    int lineNumber = 0;
    while(!in.atEnd())
    {
        QString line = in.readLine();
        if(line.contains("require") and not line.startsWith("#") and line.contains(require))
        {
            int pos = require.indexIn(line);
            if(pos > -1)
            {
                QString newRequirePath = basePath+require.cap(1);
                qDebug() << "line: " << line << " req: " << require.cap(1);

                if(findSubInFiles(basePath+"/PsScheduler/"+require.cap(1)+".rtm")
                   || findSubInFiles(basePath+"/PsScheduler/"+require.cap(1)+".flx")
                   || findSubInFiles(basePath+"/PsScheduler/PsCommon/"+require.cap(1)+".rtm")
                   || findSubInFiles(basePath+"/PsScheduler/PsCommon/"+require.cap(1)+".flx"))
                {

                }
                else
                {
                    qDebug() << "filePath: " << filePath << " line: " << line << " req: " << require.cap(1) << " file not found: " << newRequirePath;
                }
            }
        }

        if(line.contains("sub") and line.contains(sub))
        {
            int pos = sub.indexIn(line);
            if(pos > -1)
            {
                Position pos{};
                pos.filePath = filePath;
                pos.lineNumber = lineNumber;
                pos.positionInBlock = sub.pos(1);
                tags->push_back(sub.cap(1), pos);
            }
        }
        lineNumber++;
    }
    return true;
}

void TestSourceEditor::openFile(Position position)
{
    qDebug() << "Open file: " << position;
    if(position.filePath.isEmpty()) return;
    QFile file(position.filePath);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        QMessageBox::warning(this, tr("Application"),
                             tr("Cannot read file %1:\n%2.")
                             .arg(QDir::toNativeSeparators(position.filePath), file.errorString()));
        return;
    }

    currentFilePath = position.filePath;
    ui->tabWidget->setTabText(0, currentFilePath.right(currentFilePath.size()-currentFilePath.lastIndexOf('/')-1));
    codeEditorWidget.clear();

    QTextStream in(&file);
    codeEditorWidget.setPlainText(in.readAll());

    QTextDocument *doc = codeEditorWidget.document();
    codeEditorWidget.moveCursor(QTextCursor::End);
    QTextBlock tb = doc->findBlockByLineNumber(position.lineNumber);

    QTextCursor cursor(tb);
    cursor.movePosition(QTextCursor::Right, QTextCursor::MoveAnchor, position.positionInBlock);
    codeEditorWidget.centerOnScroll();
    codeEditorWidget.setTextCursor(cursor);

    statusBar()->showMessage("File loaded: "+currentFilePath, 2000);
}

void TestSourceEditor::saveCurrentViewToFile()
{
    QFile file(currentFilePath);
    if (file.open(QIODevice::WriteOnly))
    {
        QTextStream stream(&file);
        stream << codeEditorWidget.toPlainText().replace("\r", "");
        stream.flush();
        file.close();
        codeEditorWidget.document()->clearUndoRedoStacks();
    }
    else
    {
        QMessageBox::critical(this, tr("Error"), tr("Nie mozna zapisac"));
        return;
    }
}

void TestSourceEditor::retagFiles()
{
    QString lastTestPath = settings.value("lastTestPath").toString();
    if(lastTestPath.isEmpty()) return;

    visitOrder->clearAll(); //decide what to do or     visitOrder->clearFromCurentPosition();
    findSubInFiles(lastTestPath);
}

void TestSourceEditor::on_actionPrev_triggered()
{
    try
    {
        Position pos = visitOrder->prev();
//        qDebug() << "Prev: " << pos;
        openFile(pos);
    }
    catch(...)
    {

    }
}

void TestSourceEditor::on_actionNext_triggered()
{
    try
    {
        Position pos = visitOrder->next();
//        qDebug() << "Next: " << pos;
        openFile(pos);
    }
    catch(...)
    {

    }
}

void TestSourceEditor::on_actionSave_triggered()
{
    saveCurrentViewToFile();
    retagFiles();
}

void TestSourceEditor::undoAvailable(bool available)
{
    qDebug() << "undoAvailable: " << available;
    if(available)
    {
        ui->tabWidget->setTabText(0, currentFilePath.split('/').last()+"*");
    }
    else
    {
        ui->tabWidget->setTabText(0, currentFilePath.split('/').last());
    }
}

void TestSourceEditor::customContextMenuRequested(const QPoint &pos)
{
    qDebug() << "customContextMenuRequested: " << pos;
    tabBarContextMenu->popup(QCursor::pos());
}

void TestSourceEditor::getCurrentOpenFileName()
{
    qDebug() << "getCurrentOpenFileName";
    QApplication::clipboard()->setText(currentFilePath.split('/').last());
}

void TestSourceEditor::getCurrentOpenFileFullPath()
{
    qDebug() << "getCurrentOpenFullPath";
    QApplication::clipboard()->setText(currentFilePath);
}

void TestSourceEditor::on_actionRefresh_file_from_disk_triggered()
{
    openFile(Position{currentFilePath});
    retagFiles();
}

bool TestSourceEditor::shouldBeSaved()
{
    if (not codeEditorWidget.document()->isModified())
    {
        return true;
    }
    const QMessageBox::StandardButton ret = QMessageBox::warning(this, "TestSourceEditor",
                                                                 "The document has been modified.\n"
                                                                 "Do you want to save your changes?",
                                                                 QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    switch (ret)
    {
        case QMessageBox::Save:
        case QMessageBox::Cancel:
            return false;
        default:
            break;
    }
    return true;
}
