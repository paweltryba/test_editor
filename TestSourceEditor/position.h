#ifndef POSITION_H
#define POSITION_H
#include <QString>
#include <ostream>
#include <QDebug>

class Position
{
public:
    Position(QString filePath = "", int lineNumber = 0, int positionInLine = 0);
    QString filePath;
    int lineNumber;
    int positionInBlock;

    bool operator==(const Position&);
};

QDebug operator<<(QDebug os, const Position &pos);

#endif // POSITION_H
