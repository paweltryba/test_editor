#include "position.h"


QDebug operator<<(QDebug os, const Position &pos)
{
    os << "File: " << pos.filePath << " Line: " << pos.lineNumber << " Position: " << pos.positionInBlock;
    return os;
}

Position::Position(QString filePath, int lineNumber, int positionInLine)
 : filePath(filePath),
   lineNumber(lineNumber),
   positionInBlock(positionInLine)
{
}

bool Position::operator==(const Position & pos)
{
    return filePath == pos.filePath &&
           lineNumber == pos.lineNumber &&
           positionInBlock == pos.positionInBlock;
}
