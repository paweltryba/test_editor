#ifndef TESTSOURCEEDITOR_H
#define TESTSOURCEEDITOR_H

#include <QMainWindow>
#include "codeeditor.h"
#include "tags.h"
#include "visitorder.h"
#include <QListWidget>
#include <QSettings>
#include <QAction>

namespace Ui {
class TestSourceEditor;
}

class TestSourceEditor : public QMainWindow
{
    Q_OBJECT

public:
    explicit TestSourceEditor(QWidget *parent = nullptr);
    ~TestSourceEditor();

private slots:
    void on_actionOpen_triggered();
    void on_actionGo_to_definition_triggered();
    void on_actionPrev_triggered();
    void on_actionNext_triggered();
    void on_actionSave_triggered();
    void undoAvailable(bool available);
    void customContextMenuRequested(const QPoint &pos);
    void getCurrentOpenFileName();
    void getCurrentOpenFileFullPath();
    void on_actionRefresh_file_from_disk_triggered();

private:
    bool findSubInFiles(QString fileName);
    void openFile(Position pos);
    void saveCurrentViewToFile();
    void retagFiles();
    void resetTagsAndVisitOrder();
    bool shouldBeSaved();

    Ui::TestSourceEditor *ui;
    CodeEditor codeEditorWidget;
    QListWidget *functionListWidget;
    Tags *tags;
    VisitOrder *visitOrder;
    QString testFilePath;
    QString currentFilePath;
    QString basePath;
    QSettings settings;
    QMenu *tabBarContextMenu;
    QAction getFileNameAction;
    QAction getFullPathAction;
};

#endif // TESTSOURCEEDITOR_H
