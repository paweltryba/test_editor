#include "testsourceeditor.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    TestSourceEditor w;
    w.show();

    return a.exec();
}
