#include "tags.h"
#include <QDebug>

Tags::Tags()
{

}

void Tags::push_back(QString tag, Position position)
{
//    qDebug() << "Tags: " << tag << " position: " << position;
    tags.insert(tag, position);
}

Position Tags::find(QString tag)
{
    auto it = tags.find(tag);
    if(it != tags.end())
    {
        return *it;
    }
    else
    {
        throw std::out_of_range(tag.append(": No founded").toStdString());
    }
}

void Tags::clear()
{
    tags.clear();
}
