#ifndef SEARCHORDER_H
#define SEARCHORDER_H
#include <QString>
#include "position.h"
#include <QMap>

class Tags
{
public:
    Tags();

    void push_back(QString tag, Position position);
    Position find(QString tag);
    void clear();

private:
    QMap<QString, Position> tags;
};

#endif // SEARCHORDER_H
