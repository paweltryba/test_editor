/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtWidgets>

#include "codeeditor.h"

CodeEditor::CodeEditor(QWidget *parent) : QPlainTextEdit(parent)
{
    highlighter = new Highlighter(document());
    lineNumberArea = new LineNumberArea(this);
    createCustomContextMenu();

    connect(this, SIGNAL(blockCountChanged(int)), this, SLOT(updateLineNumberAreaWidth(int)));
    connect(this, SIGNAL(updateRequest(QRect,int)), this, SLOT(updateLineNumberArea(QRect,int)));
//    connect(this, SIGNAL(cursorPositionChanged()), this, SLOT(highlightCurrentLine()));
    connect(this, SIGNAL(selectionChanged()), this, SLOT(selectionChanged()));

    updateLineNumberAreaWidth(2);
//    highlightCurrentLine();
}

int CodeEditor::lineNumberAreaWidth()
{
    int digits = 1;
    int max = qMax(1, blockCount());
    while (max >= 10)
    {
        max /= 10;
        ++digits;
    }

    int space = 3 + fontMetrics().horizontalAdvance(QLatin1Char('9')) * digits;

    return space;
}

void CodeEditor::mouseDoubleClickEvent(QMouseEvent *)
{
    auto tc = textCursor();
    tc.select(QTextCursor::WordUnderCursor);
    auto selectedWord = tc.selectedText();
    qDebug() << "mouseDoubleClickEvent selected word: " << selectedWord;
    highlighter->setIsEnabledDynamicStringHiglight(selectedWord);

//    QList<QTextEdit::ExtraSelection> extraSelections;
//    QTextEdit::ExtraSelection selection;
//    QColor lineColor = QColor(Qt::yellow).lighter(160);
//    selection.format.setBackground(lineColor);
//    selection.format.setProperty(QTextFormat::FullWidthSelection, true);
//    selection.cursor = tc;
//    selection.cursor.clearSelection();
//    extraSelections.append(selection);

//    if(selectedWord.length() > 0)
//    {
//        QTextEdit::ExtraSelection currentWord;
//        currentWord.format.setBackground(QColor(Qt::green).lighter(80));
//        currentWord.cursor = tc;
//        extraSelections.append(currentWord);
//    }
//    setExtraSelections(extraSelections);
}

void CodeEditor::mousePressEvent(QMouseEvent *e)
{
    QPlainTextEdit::mousePressEvent(e);
    if (e->buttons() == Qt::RightButton)
    {
        //this part is only to show cursor after right click
        QTextCursor cursor = cursorForPosition(e->pos());
        cursor.clearSelection();
        setTextCursor(cursor);

//        cursor.select(QTextCursor::WordUnderCursor);
//        auto selectedWord = cursor.selectedText();
//        qDebug() << "selected word: " << selectedWord;
    }
}

void CodeEditor::contextMenuEvent(QContextMenuEvent *event)
{
    QMenu *menu = createStandardContextMenu();
    QMenu *styleTokenMenu = menu->addMenu("Style token");
    styleTokenMenu->addAction(use1stStyleAction);

    QMenu *removeStyleMenu = menu->addMenu("Remove style");
    removeStyleMenu->addAction(clear1stStyleAction);

    menu->exec(event->globalPos());
}

void CodeEditor::paintEvent(QPaintEvent *e)
{
    qDebug() << "paintEvent";
    QPlainTextEdit::paintEvent(e);

    QPainter painter(viewport());
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.fillRect(200, 200, 20, 20, QColor(Qt::black));
    viewport()->update();
}

void CodeEditor::setStyl1()
{
    qDebug() << "setSeyl1";
    auto tc = textCursor();
    tc.select(QTextCursor::WordUnderCursor);
    auto selectedWord = tc.selectedText();
    qDebug() << "on_actionUsing_1st_Style_triggered: " << selectedWord;

    if(selectedWord.length() > 0)
    {
        highlighter->setStyleToken1(selectedWord);
    }
}

void CodeEditor::clearStyl1()
{
    qDebug() << "clearStyl1";
    highlighter->clearStyleToken1();
}

void CodeEditor::updateLineNumberAreaWidth(int /* newBlockCount */)
{
    setViewportMargins(lineNumberAreaWidth(), 0, 0, 0);
}

void CodeEditor::updateLineNumberArea(const QRect &rect, int dy)
{
    if (dy)
        lineNumberArea->scroll(0, dy);
    else
        lineNumberArea->update(0, rect.y(), lineNumberArea->width(), rect.height());

    if (rect.contains(viewport()->rect()))
        updateLineNumberAreaWidth(0);
}

void CodeEditor::selectionChanged()
{
    auto tc = textCursor();
    tc.select(QTextCursor::WordUnderCursor);
    auto selectedWord = tc.selectedText();
//    qDebug() << "selectionChanged: " << selectedWord;
    highlighter->setIsEnabledDynamicStringHiglight("");

//    QList<QTextEdit::ExtraSelection> extraSelections;
//    QTextEdit::ExtraSelection selection;
//    QColor lineColor = QColor(Qt::yellow).lighter(160);
//    selection.format.setBackground(lineColor);
//    selection.format.setProperty(QTextFormat::FullWidthSelection, true);
//    selection.cursor = tc;
//    selection.cursor.clearSelection();
//    extraSelections.append(selection);

//    if(selectedWord.length() > 0)
//    {
//        QTextEdit::ExtraSelection currentWord;
//        currentWord.format.setBackground(QColor(Qt::green).lighter(80));
//        currentWord.cursor = tc;
//        extraSelections.append(currentWord);
//    }
//    setExtraSelections(extraSelections);
}

void CodeEditor::createCustomContextMenu()
{
    use1stStyleAction = new QAction("Using 1st Style", this);
    connect(use1stStyleAction, &QAction::triggered, this, &CodeEditor::setStyl1);

    clear1stStyleAction = new QAction("Clear 1st Style", this);
    connect(clear1stStyleAction, &QAction::triggered, this, &CodeEditor::clearStyl1);
}

void CodeEditor::resizeEvent(QResizeEvent *e)
{
    QPlainTextEdit::resizeEvent(e);

    QRect cr = contentsRect();
    lineNumberArea->setGeometry(QRect(cr.left(), cr.top(), lineNumberAreaWidth(), cr.height()));
}

void CodeEditor::highlightCurrentLine()
{
    qDebug() << "highlightCurrentLine";
    QList<QTextEdit::ExtraSelection> extraSelections;

    if (!isReadOnly())
    {
        QTextEdit::ExtraSelection selection;
        QColor lineColor = QColor(Qt::yellow).lighter(160);
        selection.format.setBackground(lineColor);
        selection.format.setProperty(QTextFormat::FullWidthSelection, true);
        selection.cursor = textCursor();
        selection.cursor.clearSelection();
        extraSelections.append(selection);
    }

//    QTextCursor cursor = textCursor();
//    cursor.select(QTextCursor::WordUnderCursor);
//    QTextEdit::ExtraSelection currentWord;
//    currentWord.format.setBackground(QColor(Qt::green).lighter(100));
//    currentWord.cursor = cursor;
//    extraSelections.append(currentWord);

    setExtraSelections(extraSelections);
}

void CodeEditor::lineNumberAreaPaintEvent(QPaintEvent *event)
{
    QPainter painter(lineNumberArea);
    painter.fillRect(event->rect(), Qt::lightGray);

    QTextBlock block = firstVisibleBlock();
    int blockNumber = block.blockNumber();
    int top = (int) blockBoundingGeometry(block).translated(contentOffset()).top();
    int bottom = top + (int) blockBoundingRect(block).height();

    while (block.isValid() && top <= event->rect().bottom())
    {
        if (block.isVisible() && bottom >= event->rect().top())
        {
            QString number = QString::number(blockNumber + 1);
            painter.setPen(Qt::black);
            painter.drawText(0, top, lineNumberArea->width(), fontMetrics().height(),
                             Qt::AlignRight, number);
        }

        block = block.next();
        top = bottom;
        bottom = top + (int) blockBoundingRect(block).height();
        ++blockNumber;
    }
}

