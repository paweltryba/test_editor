#ifndef VISITORDER_H
#define VISITORDER_H

#include <QString>
#include "position.h"
#include <QListWidget>

class VisitOrder
{
public:
    VisitOrder(QListWidget *listWidget);
    void push_back(Position position, QString tag);
    Position prev();
    Position next();
    void clearAll();
    void clearFromCurentPosition();

private:
    void print();

    using PositionVector = QVector<Position>;
    using PositionVectorIt = QVector<Position>::Iterator;
    PositionVector positions;
    int curentPos = 0;
    QListWidget *listWidget;
};

#endif // VISITORDER_H
